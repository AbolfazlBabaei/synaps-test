import React from "react";
import RouteMain from "./Router/RouteMain";

const App = () => {
  return (
    <>
      <RouteMain />
    </>
  );
};

export default App;
