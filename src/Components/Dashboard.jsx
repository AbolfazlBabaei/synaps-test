import React from "react";

import NavbarSection from "./NavbarSection";
import Panel from "./Panel";

const Dashboard = () => {
  return (
    <>
      <NavbarSection />
      <Panel />
    </>
  );
};

export default Dashboard;
