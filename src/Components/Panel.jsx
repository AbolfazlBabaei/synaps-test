import React, { useState, useEffect } from "react";
import ContentPanel from "./ContentPanel";

import MenuPanel from "./MenuPanel";
const Panel = () => {
  return (
    <>
      <div className="Panel">
        <div className="container">
          <div className="bg-white overflow_x_h">
            <div className="row">
              <span className="heaer_text">
                HISTORY TAKING AND PYHSICAL EXAMINATION
              </span>
            </div>
            <hr className="hr_border" />
            <div className="row mb_panel">
              <div className="col-4 border_r">
                <MenuPanel />
              </div>
              <div className="col-8">
                <ContentPanel />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Panel;
