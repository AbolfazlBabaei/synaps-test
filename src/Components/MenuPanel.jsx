import React, { useState } from "react";
import LocalHospitalIcon from "@material-ui/icons/LocalHospital";

const MenuPanel = () => {
  //State
  const [isActive, setIsActive] = useState("Patient Profile");
  const [list, setList] = useState([
    {
      title: "Patient Profile",
      icon: <LocalHospitalIcon className="ms-2" />,
    },
    {
      title: "Chief Complaint(s)",
    },
    {
      title: "Present Illness",
    },
    {
      title: "Drug history",
    },
    {
      title: "Past History",
    },
    {
      title: "Family History",
    },
    {
      title: "Personal and Social History",
    },
    {
      title: "Family History",
    },
    {
      title: "Review Of Systems",
    },
    {
      title: "The Physical Examination",
    },
    {
      title: "Problem list",
    },
    {
      title: "DDx",
    },
    {
      title: "Plan and assessment",
    },
    {
      title: "DX",
    },
    {
      title: "Management",
    },
    {
      title: "Follow up",
    },
    {
      title: "Cash",
    },
  ]);
  return (
    <>
      <div className="px-4 pt-5 ">
        {list.map((item, index) => {
          return (
            <>
              <div
                onClick={() => setIsActive(item.title)}
                key={index}
                className={`Menu_item mb-2 d-flex ${
                  isActive === item.title ? "Menu_item_active" : ""
                }`}
              >
                {item.icon ? item.icon : <div className="ms-2">     </div>}
                <span className="fw_600 fs-09em ms-4">{item.title}</span>
                {isActive === item.title && <div className="triangle"></div>}
              </div>
            </>
          );
        })}
      </div>
    </>
  );
};

export default MenuPanel;
