import React from "react";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";

const ContentPanel = () => {
  // State
  const [selectedDate, setSelectedDate] = React.useState(
    new Date("2021-07-21T21:11:54")
  );

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };
  return (
    <>
      <div className="row">
        <div className="mt-5 ms-5">
          <span className="fw_600 fs-13em">Edit Your Profile</span>
        </div>
        <div className="d-flex justify-content-center align-items-center mt-3">
          <span className="fw_600 me-2">Age:</span>
          <TextField
            id="date"
            // label="Birthday"
            type="date"
            defaultValue="2021-07-21"
            className="w-75 "
            InputLabelProps={{
              shrink: true,
            }}
          />
        </div>
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-center align-items-center mt-1">
          <span className="fw_600 me-2">Gender:</span>
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Male"
          />
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Female"
          />
        </div>
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-center align-items-center mt-1">
          <span className="fw_600 me-2">Marital Status:</span>
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Single"
          />
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Married"
          />
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Widow"
          />
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Widower"
          />
        </div>{" "}
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-center align-items-center">
          <span className="fw_600 me-2">Occupation:</span>
          <TextField
            className="mb-3 w-50"
            id="standard-basic"
            label="Occupation"
          />
        </div>
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex ps-5 mt-3">
          <span className="fw_600 me-2">
            Source Of History {"(oR rEFERRAL)"}:
          </span>
          <div className="d-flex flex-column">
            <FormControlLabel
              value="end"
              control={<Radio color="primary" />}
              label="patient"
            />
            <FormControlLabel
              value="end"
              control={<Radio color="primary" />}
              label="family members"
            />
            <div className="ps-5">
              <FormControlLabel
                value="end"
                control={<Radio color="primary" />}
                label="father"
              />
              <FormControlLabel
                value="end"
                control={<Radio color="primary" />}
                label="mother"
              />
              <FormControlLabel
                value="end"
                control={<Radio color="primary" />}
                label="sister"
              />
              <FormControlLabel
                value="end"
                control={<Radio color="primary" />}
                label="brother"
              />
            </div>
            <FormControlLabel
              value="end"
              control={<Radio color="primary" />}
              label="friends"
            />
            <FormControlLabel
              value="end"
              control={<Radio color="primary" />}
              label="An offiver"
            />
            <FormControlLabel
              value="end"
              control={<Radio color="primary" />}
              label="consultant"
            />
          </div>
        </div>
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-center align-items-center">
          <span className="fw_600 me-2">others:</span>
          <TextField className="mb-3 w-50" id="standard-basic" label="others" />
        </div>
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-center align-items-center">
          <span className="fw_600 me-2">comment:</span>
          <TextField
            className="mb-3 w-50"
            id="standard-basic"
            label="comment"
          />
        </div>
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-center align-items-center mt-1">
          <span className="fw_600 me-2">Reliability:</span>
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="Yes"
          />
          <FormControlLabel
            value="end"
            control={<Radio color="primary" />}
            label="No"
          />
        </div>{" "}
        <hr className="hr_border mt-3"></hr>
        <div className="d-flex justify-content-end">
          <Button
            className="Next_Step py-2"
            variant="contained"
            color="primary"
          >
            Next Step
          </Button>
        </div>
      </div>
    </>
  );
};

export default ContentPanel;
