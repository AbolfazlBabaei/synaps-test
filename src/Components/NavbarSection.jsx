import React, { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import SignalCellularAltIcon from "@material-ui/icons/SignalCellularAlt";
import WidgetsIcon from "@material-ui/icons/Widgets";
import SearchIcon from "@material-ui/icons/Search";
import ControlCameraOutlinedIcon from "@material-ui/icons/ControlCameraOutlined";
import HomeIcon from "@material-ui/icons/Home";
import MenuIcon from "@material-ui/icons/Menu";

const NavbarSection = () => {
  // State
  const [isActive, setIsActive] = useState("Dashboard");
  const [isMobile, setIsMobile] = useState(false);
  const [menuIsOpen, setMenuIsOpen] = useState(false);
  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth > 992) {
        setIsMobile(false);
      } else {
        setIsMobile(true);
      }
    };
    window.addEventListener("resize", handleResize);
  }, [window]);

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth > 992) {
        setIsMobile(false);
      } else {
        setIsMobile(true);
      }
    };
    handleResize();
  }, []);
  return (
    <>
      <div className="Navbar_Section">
        <div className="container">
          <div className="row pt-3">
            <div className="col-7 d-none d-lg-flex justify-content-start align-items-center">
              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("Dashboard")}
                  className={`btn_style ms-5 me-4 ${
                    isActive === "Dashboard" && "active_nav_btn"
                  }`}
                >
                  Dashboard
                </Button>
              </div>
              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("Health")}
                  className={`btn_style ${
                    isActive === "Health" && "active_nav_btn"
                  }`}
                >
                  Health
                </Button>
              </div>

              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("Parking")}
                  className={`btn_style ${
                    isActive === "Parking" && "active_nav_btn"
                  }`}
                >
                  Parking
                </Button>
              </div>

              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("Car wash")}
                  className={`btn_style ${
                    isActive === "Car wash" && "active_nav_btn"
                  }`}
                >
                  Car wash
                </Button>
              </div>

              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("Hotel")}
                  className={`btn_style ${
                    isActive === "Hotel" && "active_nav_btn"
                  }`}
                >
                  Hotel
                </Button>
              </div>

              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("Groceries")}
                  className={`btn_style ${
                    isActive === "Groceries" && "active_nav_btn"
                  }`}
                >
                  Groceries
                </Button>
              </div>

              <div className="nav_btn">
                <Button
                  onClick={() => setIsActive("WSA")}
                  className={`btn_style ${
                    isActive === "WSA" && "active_nav_btn"
                  }`}
                >
                  WSA
                </Button>
              </div>
            </div>

            <div className="col-12 col-lg-5 d-flex justify-content-between justify-content-lg-end">
              {isMobile && (
                <div className="d-flex align-items-center">
                  <div
                    onClick={() => setMenuIsOpen(!menuIsOpen)}
                    className="mx-3 pointer"
                  >
                    <MenuIcon className="icon_transparent" />
                  </div>
                </div>
              )}
              <div className="d-flex justify-content-end align-items-center">
                <div className="mx-3 pointer">
                  <SearchIcon className="icon_transparent" />
                </div>
                <div className="mx-3 pointer">
                  <ControlCameraOutlinedIcon className="icon_transparent" />
                </div>
                <div className="mx-3 pointer">
                  <WidgetsIcon className="icon_transparent" />
                </div>
                <div className="mx-3 pointer">
                  <SignalCellularAltIcon className="icon_transparent" />
                </div>
                <div className="mx-3 pointer">
                  <span className="text_transparent fs-07em">Hi,</span>
                  <span className="name">Mehran</span>
                </div>
                <div className="Profile_Pic">
                  <span>M</span>
                </div>
              </div>
            </div>

            {/* <div className="d-flex justify-content-between align-items-center mt-4"></div> */}
          </div>
        </div>
        <hr className="text-white" />
        <div
          className={`Mobile_Menu ${menuIsOpen ? "open_menu" : "close_menu"}`}
        >
          <div className="Menu_div">
            <div className="d-flex">
              <div className="nav_btn w-50">
                <Button
                  onClick={() => setIsActive("Dashboard")}
                  className={`btn_style w-100 ms-lg-5 me-lg-4 ${
                    isActive === "Dashboard" && "active_nav_btn"
                  }`}
                >
                  Dashboard
                </Button>
              </div>
              <div className="nav_btn w-50">
                <Button
                  onClick={() => setIsActive("Health")}
                  className={`btn_style w-100 ${
                    isActive === "Health" && "active_nav_btn"
                  }`}
                >
                  Health
                </Button>
              </div>
            </div>
            <div className="d-flex">
              <div className="nav_btn w-50">
                <Button
                  onClick={() => setIsActive("Parking")}
                  className={`btn_style w-100 ${
                    isActive === "Parking" && "active_nav_btn"
                  }`}
                >
                  Parking
                </Button>
              </div>

              <div className="nav_btn w-50">
                <Button
                  onClick={() => setIsActive("Car wash")}
                  className={`btn_style w-100 ${
                    isActive === "Car wash" && "active_nav_btn"
                  }`}
                >
                  Car wash
                </Button>
              </div>
            </div>
            <div className="d-flex">
              <div className="nav_btn w-50">
                <Button
                  onClick={() => setIsActive("Hotel")}
                  className={`btn_style w-100 ${
                    isActive === "Hotel" && "active_nav_btn"
                  }`}
                >
                  Hotel
                </Button>
              </div>

              <div className="nav_btn w-50">
                <Button
                  onClick={() => setIsActive("Groceries")}
                  className={`btn_style w-100 ${
                    isActive === "Groceries" && "active_nav_btn"
                  }`}
                >
                  Groceries
                </Button>
              </div>
            </div>

            <div className="nav_btn">
              <Button
                onClick={() => setIsActive("WSA")}
                className={`btn_style w-100 ${
                  isActive === "WSA" && "active_nav_btn"
                }`}
              >
                WSA
              </Button>
            </div>
          </div>
        </div>

        <div className="container">
          <div className="row mt-lg-5 mt-2">
            <div className="col-12 col-lg-6">
              <div>
                <span className="fs-15em d-block text-white">Dashboard</span>
                <div className="d-flex text-white align-items-center">
                  <HomeIcon fontSize="small" />
                  <ul className="d-flex list">
                    <li>Dashboard</li>
                    <li className="ms-4">Default Dashboard</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-6 d-flex justify-content-center  justify-content-lg-end align-items-center mt-4 mt-lg-0">
              <div className="btn_reports">
                <Button className="btn_style_Reports me-2">Reports</Button>
              </div>
              <div>
                <Button
                  className="btn_style_Action bg-white"
                  variant="contained"
                >
                  Action
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default NavbarSection;
